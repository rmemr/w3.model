//
// model::debug
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
#[derive(Clone)]
pub enum DebugValue {
    String(String, String),
    Name(String, String),
    Int(String, i32),
    Float(String, f32),
    Vec(String, Vec<DebugValue>),
}
// ----------------------------------------------------------------------------
#[derive(Clone, Copy)]
pub enum DebugCategory {
    LayerStatic,
    LayerInteractive,
    LayerMapPin,
    LayerArea,
    LayerWaypoint,
    LayerScenepoint,
    LayerActionpoint,
    LayerWanderpoint,
    Layer,
    Community,
    NavMesh,
}
// ----------------------------------------------------------------------------
#[derive(Clone, Default)]
pub struct DebugInfo(pub Vec<DebugValue>);
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
impl DebugCategory {
    // ------------------------------------------------------------------------
    pub fn as_str(self) -> &'static str {
        match self {
            DebugCategory::LayerStatic => "layer_static",
            DebugCategory::LayerInteractive => "layer_interactive",
            DebugCategory::LayerMapPin => "layer_mappin",
            DebugCategory::LayerArea => "layer_area",
            DebugCategory::LayerWaypoint => "layer_waypoint",
            DebugCategory::LayerScenepoint => "layer_scenepoint",
            DebugCategory::LayerActionpoint => "layer_actionpoint",
            DebugCategory::LayerWanderpoint => "layer_wanderpoint",
            DebugCategory::Layer => "layer",
            DebugCategory::Community => "community",
            DebugCategory::NavMesh => "navmesh",
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl DebugInfo {
    // ------------------------------------------------------------------------
    pub fn new(category: DebugCategory) -> DebugInfo {
        let mut d = DebugInfo(Vec::new());
        d.add_str("type", category.as_str());
        d
    }
    // ------------------------------------------------------------------------
    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }
    // ------------------------------------------------------------------------
    pub fn clear(&mut self) {
        self.0.clear();
    }
    // ------------------------------------------------------------------------
    pub fn append(&mut self, mut additional: DebugInfo) -> &mut Self {
        self.0.append(&mut additional.0);
        self
    }
    // ------------------------------------------------------------------------
    pub fn add(&mut self, value: DebugValue) -> &mut Self {
        self.0.push(value);
        self
    }
    // ------------------------------------------------------------------------
    pub fn build(&mut self) -> Self {
        self.clone()
    }
    // ------------------------------------------------------------------------
    pub fn add_str<T: Into<String>>(&mut self, key: &str, value: T) -> &mut Self {
        self.0.push(DebugValue::String(key.into(), value.into()));
        self
    }
    // ------------------------------------------------------------------------
    pub fn add_str_opt(&mut self, key: &str, value: Option<&str>) -> &mut Self {
        if let Some(value) = value {
            self.0.push(DebugValue::String(key.into(), value.into()));
        }
        self
    }
    // ------------------------------------------------------------------------
    pub fn add_string_opt(&mut self, key: &str, value: Option<&String>) -> &mut Self {
        if let Some(value) = value {
            self.0.push(DebugValue::String(key.into(), value.clone()));
        }
        self
    }
    // ------------------------------------------------------------------------
    pub fn add_name(&mut self, key: &str, value: &str) -> &mut Self {
        self.0.push(DebugValue::Name(key.into(), value.into()));
        self
    }
    // ------------------------------------------------------------------------
    pub fn add_int(&mut self, key: &str, value: i32) -> &mut Self {
        self.0.push(DebugValue::Int(key.into(), value));
        self
    }
    // ------------------------------------------------------------------------
    pub fn add_float(&mut self, key: &str, value: f32) -> &mut Self {
        self.0.push(DebugValue::Float(key.into(), value));
        self
    }
    // ------------------------------------------------------------------------
    pub fn add_float_opt(&mut self, key: &str, value: Option<f32>) -> &mut Self {
        if let Some(value) = value {
            self.0.push(DebugValue::Float(key.into(), value));
        }
        self
    }
    // ------------------------------------------------------------------------
    pub fn add_bool(&mut self, key: &str, value: bool) -> &mut Self {
        self.0
            .push(DebugValue::Int(key.into(), if value { 1 } else { -1 }));
        self
    }
    // ------------------------------------------------------------------------
    pub fn add_vec(&mut self, key: &str, value: Vec<DebugValue>) -> &mut Self {
        self.0.push(DebugValue::Vec(key.into(), value));
        self
    }
    // ------------------------------------------------------------------------
    pub fn get_str(&self, searched_key: &str) -> Option<&str> {
        for v in &self.0 {
            match *v {
                DebugValue::String(ref key, ref value) if key == searched_key => return Some(value),
                _ => {}
            }
        }
        None
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl DebugValue {
    // ------------------------------------------------------------------------
    pub fn new_str<S: Into<String>>(id: S, value: S) -> DebugValue {
        DebugValue::String(id.into(), value.into())
    }
    // ------------------------------------------------------------------------
    pub fn new_name<S: Into<String>>(id: S, value: S) -> DebugValue {
        DebugValue::Name(id.into(), value.into())
    }
    // ------------------------------------------------------------------------
    pub fn new_int<S: Into<String>>(id: S, value: i32) -> DebugValue {
        DebugValue::Int(id.into(), value)
    }
    // ------------------------------------------------------------------------
    pub fn new_float<S: Into<String>>(id: S, value: f32) -> DebugValue {
        DebugValue::Float(id.into(), value)
    }
    // ------------------------------------------------------------------------
    pub fn new_bool<S: Into<String>>(id: S, value: bool) -> DebugValue {
        DebugValue::Int(id.into(), if value { 1 } else { -1 })
    }
    // ------------------------------------------------------------------------
    pub fn new_vec<S: Into<String>>(id: S, value: Vec<DebugValue>) -> DebugValue {
        DebugValue::Vec(id.into(), value)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
