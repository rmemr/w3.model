//
// journal::quest
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
#[derive(Model)]
#[model(getr)]
pub struct QuestGroupJournal {
    id: String,
    guid: GUID,
}
// ----------------------------------------------------------------------------
#[derive(Model, ModelRepo)]
pub struct QuestJournal {
    #[model(getr)]
    id: String,
    #[model(getr)]
    guid: GUID,
    #[model(getr, set)]
    title: String,
    #[model(getr, set)]
    questtype: String, //TODO as enum
    #[model(getr)]
    world: Option<String>,
    #[model(getr, set)]
    level: Option<u8>,
    #[model(getr)]
    description: JournalEntryGroup,
    #[modelrepo(getr)]
    instructions: BTreeMap<String, QuestJournalPhase>,
}
// ----------------------------------------------------------------------------
#[derive(Model, ModelRepo)]
pub struct QuestJournalPhase {
    #[model(getr)]
    id: String,
    #[model(getr)]
    guid: GUID,
    #[modelrepo(getr)]
    objectives: BTreeMap<String, QuestJournalObjective>,
}
// ----------------------------------------------------------------------------
#[derive(Model, ModelRepo)]
pub struct QuestJournalObjective {
    #[model(getr)]
    id: String,
    #[model(getr)]
    guid: GUID,
    #[model(getr)]
    pos: u8,
    #[model(getr, set)]
    caption: String,
    #[model(getr)]
    world: Option<String>,
    #[modelrepo(getr)]
    mappins: BTreeMap<String, QuestJournalMapPin>,
}
// ----------------------------------------------------------------------------
#[derive(Model)]
#[model(getr)]
pub struct QuestJournalMapPin {
    id: String,
    guid: GUID,
    pos: u8,
    #[model(getr, set)]
    radius: Option<f32>,
    _activate: Option<bool>,
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::collections::BTreeMap;

use primitives::GUID;

use journal::JournalEntryGroup;
// ----------------------------------------------------------------------------
impl QuestGroupJournal {
    // ------------------------------------------------------------------------
    pub fn new(id: &str, guid: &GUID) -> QuestGroupJournal {
        QuestGroupJournal {
            id: id.to_lowercase(),
            guid: guid.clone(),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl QuestJournal {
    // ------------------------------------------------------------------------
    pub fn new(id: &str) -> QuestJournal {
        QuestJournal {
            id: id.to_lowercase(),
            guid: GUID::new(),
            title: String::default(),
            questtype: "Side".to_string(),
            world: None,
            level: None,
            description: JournalEntryGroup::default(),
            instructions: BTreeMap::new(),
        }
    }
    // ------------------------------------------------------------------------
    pub fn set_world(&mut self, worldid: &str, err_id: &str) -> Result<()> {
        self.world = match worldid.to_lowercase().as_str() {
            "multiple" => None,
            _ => {
                let worldid = worldid.to_lowercase();
                // inject worldid into every phase objective if and ONLY IF it
                // has no world set (-> Error)
                for phase in self.instructions.values_mut() {
                    phase.set_world(&worldid, err_id)?;
                }
                Some(worldid)
            }
        };
        Ok(())
    }
    // ------------------------------------------------------------------------
    pub fn add_description(&mut self, id: &str, text: &str) {
        self.description.add(id, text)
    }
    // ------------------------------------------------------------------------
    pub fn add_phase(&mut self, phaseid: &str, phase: QuestJournalPhase) {
        self.instructions.insert(phaseid.to_lowercase(), phase);
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl QuestJournalPhase {
    // ------------------------------------------------------------------------
    pub fn new(id: &str) -> QuestJournalPhase {
        QuestJournalPhase {
            id: id.to_lowercase(),
            guid: GUID::new(),
            objectives: BTreeMap::new(),
        }
    }
    // ------------------------------------------------------------------------
    pub fn add_objective(&mut self, id: &str, mut objective: QuestJournalObjective) -> &mut Self {
        objective.set_pos(self.objectives.len() as u8);
        self.objectives.insert(id.to_lowercase(), objective);
        self
    }
    // ------------------------------------------------------------------------
    fn set_world(&mut self, world: &str, err_id: &str) -> Result<()> {
        for (objectiveid, objective) in &mut self.objectives {
            if objective.has_world() {
                return Err(format!(
                    "{}/{}/{}: (re)defining world in objectives not supported for single world quests.",
                    err_id, &self.id, objectiveid
                ));
            }
            objective.set_world(world);
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    fn validate_world(&self, id: &str) -> Result<()> {
        for (objectiveid, objective) in &self.objectives {
            if !objective.has_world() {
                return Err(format!(
                    "{}: multi world quests require world definition at \
                     every objective. found objective without world definition: {}/{}",
                    id, &self.id, objectiveid
                ));
            }
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl QuestJournalObjective {
    // ------------------------------------------------------------------------
    pub fn new(id: &str) -> QuestJournalObjective {
        QuestJournalObjective {
            pos: 0,
            id: id.to_lowercase(),
            guid: GUID::new(),
            caption: String::default(),
            world: None,
            mappins: BTreeMap::new(),
        }
    }
    // ------------------------------------------------------------------------
    pub fn new_with_caption(name: &str, caption: &str) -> QuestJournalObjective {
        let mut objective = QuestJournalObjective::new(name);
        objective.set_caption(caption);
        objective
    }
    // ------------------------------------------------------------------------
    pub fn set_world(&mut self, world: &str) {
        self.world = Some(world.to_lowercase());
    }
    // ------------------------------------------------------------------------
    pub fn add_mappin(&mut self, mut mappin: QuestJournalMapPin) {
        mappin.set_pos(self.mappins.len() as u8);
        self.mappins.insert(mappin.id.to_lowercase(), mappin);
    }
    // ------------------------------------------------------------------------
    fn has_world(&self) -> bool {
        self.world.is_some()
    }
    // ------------------------------------------------------------------------
    fn set_pos(&mut self, pos: u8) {
        self.pos = pos;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl QuestJournalMapPin {
    // ------------------------------------------------------------------------
    pub fn new(id: &str) -> QuestJournalMapPin {
        QuestJournalMapPin {
            id: id.to_lowercase(),
            guid: GUID::new(),
            pos: 0,
            radius: None,
            _activate: None,
        }
    }
    // ------------------------------------------------------------------------
    fn set_pos(&mut self, pos: u8) {
        self.pos = pos;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Validation
// ----------------------------------------------------------------------------
use {Result, ValidatableElement};
// ----------------------------------------------------------------------------
impl ValidatableElement for QuestJournal {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        if self.title.is_empty() {
            validate_err_missing!(id, "title")
        } else if self.description.is_empty() {
            validate_err_missing!(id, "description")
        } else if self.instructions.is_empty() {
            validate_err_missing!(id, "instructions")
        } else {
            // if the quest is multi-world a world must be defined for ALL objectives
            // explicitely
            if self.world.is_none() {
                for phase in self.instructions.values() {
                    phase.validate_world(id)?
                }
            }
            Ok(())
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for QuestJournalPhase {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        if self.objectives.is_empty() {
            validate_err_missing!(id, "objective")
        } else {
            Ok(())
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for QuestJournalObjective {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        if self.caption.is_empty() {
            validate_err_missing!(id, "caption")
        } else {
            Ok(())
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for QuestJournalMapPin {
    // ------------------------------------------------------------------------
    fn validate(&self, _: &str) -> Result<()> {
        // nothing to check (?)
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
