//
// model::layer
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
#[derive(Default, ModelRepo)]
pub struct QuestLayers {
    #[modelrepo(getr, add, has)]
    layers: IndexMap<String, QuestLayer>,
}
// ----------------------------------------------------------------------------
pub enum QuestLayer {
    FilePath(String, String),
    Data(LayerContent),
}
// ----------------------------------------------------------------------------
#[derive(Model)]
pub struct LayerContent {
    #[model(getr)]
    id: String,
    #[model(getr, set)]
    world: Option<String>,
    #[model(getr, set)]
    context: Option<String>,
    #[model(getr, getmr)]
    content: Vec<entities::LayerContentEntity>,
}
// ----------------------------------------------------------------------------
#[derive(Model)]
pub struct LayerInfo {
    #[model(getr)]
    name: String,
    #[model(getr)]
    filepath: String,
    #[model(getr)]
    guid: GUID,
    #[model(getr)]
    variant: LayerVariant,
    #[model(getr)]
    specialization: LayerSpecialization,
}
// ----------------------------------------------------------------------------
pub mod entities;
// ----------------------------------------------------------------------------
use indexmap::map::{Drain, IndexMap};

use primitives::enums::{LayerSpecialization, LayerVariant};
use primitives::GUID;

use {Result, ValidatableElement};
// ----------------------------------------------------------------------------
impl QuestLayers {
    // ------------------------------------------------------------------------
    pub fn drain(&mut self) -> Drain<String, QuestLayer> {
        self.layers.drain(..)
    }
    // ------------------------------------------------------------------------
    pub fn layer_ids(&self) -> impl Iterator<Item = &String> {
        self.layers.keys()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl LayerContent {
    // ------------------------------------------------------------------------
    pub fn new<S: Into<String>>(id: S) -> LayerContent {
        LayerContent {
            id: id.into(),
            world: None,
            context: None,
            content: Vec::default(),
        }
    }
    // ------------------------------------------------------------------------
    pub fn new_from<S: Into<String>>(id: S, src: &LayerContent) -> LayerContent {
        LayerContent {
            id: id.into(),
            world: src.world.clone(),
            context: src.context.clone(),
            content: Vec::new(),
        }
    }
    // ------------------------------------------------------------------------
    pub fn add_entity<E: Into<entities::LayerContentEntity>>(&mut self, entity: E) {
        self.content.push(entity.into());
    }
    // ------------------------------------------------------------------------
    pub fn clear_debuginfo(&mut self) {
        for entity in &mut self.content {
            entity.clear_debuginfo();
        }
    }
    // ------------------------------------------------------------------------
    pub fn generate_debuginfo(&mut self, questid: &str) -> Result<()> {
        for entity in &mut self.content {
            entity.generate_debuginfo(
                questid,
                &self.id,
                self.context.as_ref().map(AsRef::as_ref),
            )?;
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl LayerInfo {
    // ------------------------------------------------------------------------
    pub fn new(name: &str, filepath: &str) -> LayerInfo {
        LayerInfo {
            name: name.to_owned(),
            filepath: filepath.replace('/', "\\"),
            guid: GUID::new(),
            variant: LayerVariant::NonStatic,
            specialization: LayerSpecialization::Quest,
        }
    }
    // ------------------------------------------------------------------------
    pub fn set_variant(&mut self, variant: LayerVariant) -> &mut Self {
        self.variant = variant;
        self
    }
    // ------------------------------------------------------------------------
    pub fn set_specialization(&mut self, specialization: LayerSpecialization) -> &mut Self {
        self.specialization = specialization;
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Validation
// ----------------------------------------------------------------------------
use validator::validate_str_default;
// ----------------------------------------------------------------------------
impl ValidatableElement for LayerContent {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        // world is set?
        if let Some(ref context) = self.context {
            validate_str_default(context, id, "^[0-9_a-z\\\\/]*$", "[a-z_0-9]")?
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
