//
// model::navdata
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
#[derive(Default, Model)]
pub struct NavData {
    #[model(getr)]
    navdatasets: IndexMap<String, HubNavData>,
}
// ----------------------------------------------------------------------------
#[derive(Default, Model)]
pub struct HubNavData {
    #[model(getr, getmr)]
    meshes: IndexMap<String, NavMesh>,
}
// ----------------------------------------------------------------------------
#[derive(Model)]
pub struct NavMesh {
    #[model(getr)]
    id: String,
    #[model(iter)]
    triangles: Vec<[Vector3D; 3]>,
    #[model(iter)]
    mergeable_borderedges: Vec<[Vector3D; 2]>,
}
// ----------------------------------------------------------------------------
use indexmap::map::{Drain, IndexMap};

use primitives::Vector3D;

use {Result, ValidatableElement};
// ----------------------------------------------------------------------------
impl NavData {
    // ------------------------------------------------------------------------
    pub fn add_navdataset<T: Into<String>>(
        &mut self,
        worldid: T,
        mut dataset: HubNavData,
    ) -> Result<()> {
        let world = worldid.into();

        let entry = self.navdatasets.entry(world.clone()).or_default();
        for (id, mesh) in dataset.meshes.drain(..) {
            entry
                .add_navmesh(id, mesh)
                .map_err(|e| format!("navdata set {}: {}", world, e))?;
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
    pub fn drain(&mut self) -> Drain<String, HubNavData> {
        self.navdatasets.drain(..)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl HubNavData {
    // ------------------------------------------------------------------------
    pub fn add_navmesh<T: Into<String>>(&mut self, id: T, mesh: NavMesh) -> Result<()> {
        let id = id.into();
        if self.meshes.contains_key(&id) {
            Err(format!(
                "navmesh name must be unique per hub. found duplicate navmesh: {}",
                id
            ))
        } else {
            self.meshes.insert(id, mesh);
            Ok(())
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl NavMesh {
    // ------------------------------------------------------------------------
    pub fn new(id: &str) -> NavMesh {
        NavMesh {
            id: id.to_owned(),
            triangles: Vec::default(),
            mergeable_borderedges: Vec::default(),
        }
    }
    // ------------------------------------------------------------------------
    pub fn add_triangle(&mut self, vertex1: Vector3D, vertex2: Vector3D, vertex3: Vector3D) {
        self.triangles.push([vertex1, vertex2, vertex3]);
    }
    // ------------------------------------------------------------------------
    pub fn add_mergeable_edge(&mut self, vertex1: Vector3D, vertex2: Vector3D) {
        self.mergeable_borderedges.push([vertex1, vertex2]);
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for HubNavData {
    // ------------------------------------------------------------------------
    fn validate(&self, _id: &str) -> Result<()> {
        Ok(())
    }
}
// ----------------------------------------------------------------------------
impl ValidatableElement for NavMesh {
    // ------------------------------------------------------------------------
    fn validate(&self, _id: &str) -> Result<()> {
        Ok(())
    }
}
// ----------------------------------------------------------------------------
