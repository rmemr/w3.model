//
// primitives::references between questgraph and different other definitions
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
#[derive(Clone, Default)]
pub struct CheckedTag(String);
// ----------------------------------------------------------------------------
#[derive(Clone)]
pub enum LayerTagReference {
    UncheckedTag(String),
    WorldAndTag(String, CheckedTag), // world id, entity tag-as-is
    WorldAndId(String, String),      // world id, entity id
}
// ----------------------------------------------------------------------------
#[derive(Clone)]
pub enum ActorReference {
    Unchecked(String),
    CommunityActor(String, String),
}
// ----------------------------------------------------------------------------
#[derive(Clone)]
pub enum EntityReference {
    Unchecked(String),
    ClassAndReference(String, String),
    ClassAndId(String, String),
}
// ----------------------------------------------------------------------------
#[derive(Clone)]
pub enum InteractionReference {
    Unchecked(String),
    LayerEntity(String, String),
}
// ----------------------------------------------------------------------------
#[derive(Clone)]
pub enum SceneReference {
    Definition(String), // relative path/to/scene.yml
    Unchecked(String),  // path/to/scene.w2scene
}
// ----------------------------------------------------------------------------
#[derive(Clone)]
pub enum ItemReference {
    Unchecked(String),
    CategoryAndName(String, String),
}
// ----------------------------------------------------------------------------
pub enum LootReference {
    Unchecked(String),
    Id(String),
}
// ----------------------------------------------------------------------------
pub enum RewardReference {
    Unchecked(String),
    Id(String),
}
// ----------------------------------------------------------------------------
impl CheckedTag {
    // ------------------------------------------------------------------------
    pub fn new<S: Into<String>>(tag: S) -> CheckedTag {
        CheckedTag(tag.into())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl InteractionReference {
    // ------------------------------------------------------------------------
    pub fn new_entity(world: &str, id: &str) -> InteractionReference {
        InteractionReference::LayerEntity(world.to_lowercase(), id.to_lowercase())
    }
    // ------------------------------------------------------------------------
    pub fn to_uid(&self) -> String {
        match self {
            InteractionReference::Unchecked(tag) => format!("unchecked:{}", tag),
            InteractionReference::LayerEntity(world, id) => format!("{}:{}", world, id),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl LayerTagReference {
    // ------------------------------------------------------------------------
    pub fn new(world: &str, tag: &str) -> LayerTagReference {
        if tag.starts_with('~') {
            LayerTagReference::WorldAndTag(
                world.to_lowercase(),
                CheckedTag(tag.split_at(1).1.to_lowercase()),
            )
        } else {
            LayerTagReference::WorldAndId(world.to_lowercase(), tag.to_lowercase())
        }
    }
    // ------------------------------------------------------------------------
    pub fn to_id(&self) -> String {
        match self {
            LayerTagReference::UncheckedTag(tag) => format!("unchecked:{}", tag),
            LayerTagReference::WorldAndId(world, id) => format!("{}:{}", world, id),
            LayerTagReference::WorldAndTag(world, tag) => format!("{}:{}", world, tag),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl RewardReference {
    // ------------------------------------------------------------------------
    pub fn to_id(&self) -> String {
        match self {
            RewardReference::Unchecked(tag) => format!("~{}", tag),
            RewardReference::Id(id) => id.to_string(),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Deref trait impl
// ----------------------------------------------------------------------------
impl std::ops::Deref for CheckedTag {
    type Target = String;
    // ------------------------------------------------------------------------
    fn deref(&self) -> &Self::Target {
        &self.0
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Display trait impl
// ----------------------------------------------------------------------------
use std::fmt;
// ----------------------------------------------------------------------------
impl fmt::Display for InteractionReference {
    // ------------------------------------------------------------------------
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.to_uid())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl fmt::Display for CheckedTag {
    // ------------------------------------------------------------------------
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "~{}", self.0)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Validation
// ----------------------------------------------------------------------------
use validator::validate_str_default as validate_str;
use {Result, ValidatableElement};
// ----------------------------------------------------------------------------
impl ValidatableElement for SceneReference {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        match self {
            SceneReference::Unchecked(scene) => {
                validate_str(scene, id, "^[0-9/._a-z]*$", "[a-z/._0-9]")?;

                if !scene.ends_with(".w2scene") {
                    validate_err!(id, "w2scene filepath must end with .w2scene");
                }
            }
            SceneReference::Definition(scene) => {
                validate_str(scene, id, "^[0-9/._a-z]*$", "[a-z/._0-9]")?;

                if scene.ends_with(".w2scene") {
                    validate_err!(id, "scene defintion must not end with .w2scene");
                }
                if scene.starts_with('/') {
                    validate_err!(id, "absolute paths for scene definitions not supported.");
                }
            }
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for LayerTagReference {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        use self::LayerTagReference::*;

        match self {
            UncheckedTag(tag) => {
                validate_str(tag, id, "^[0-9_a-zA-Z]*$", "[a-zA-Z_0-9]")?;
            }
            WorldAndTag(world, CheckedTag(tag)) | WorldAndId(world, tag) => {
                validate_str(world, &format!("{} world", id), "^[0-9_a-z]*$", "[a-z_0-9]")?;
                validate_str(tag, &format!("{} tag", id), "^[0-9_a-z]*$", "[a-z_0-9]")?;
            }
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for InteractionReference {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        use self::InteractionReference::*;

        match self {
            Unchecked(tag) => {
                validate_str(tag, id, "^[0-9_a-zA-Z]*$", "[a-zA-Z_0-9]")?;
            }
            LayerEntity(world, id) => {
                validate_str(world, &format!("{} world", id), "^[0-9_a-z]*$", "[a-z_0-9]")?;
                validate_str(
                    id,
                    &format!("{} entity-id", id),
                    "^[0-9_a-z]*$",
                    "[a-z_0-9]",
                )?;
            }
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for RewardReference {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        use self::RewardReference::*;

        match self {
            Unchecked(tag) => {
                validate_str(tag, id, "^[0-9_a-zA-Z]*$", "[a-z_0-9]")?;
            }
            Id(id) => {
                validate_str(
                    id,
                    &format!("{} reward-id", id),
                    "^[0-9_a-z]*$",
                    "[a-z_0-9]",
                )?;
            }
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
