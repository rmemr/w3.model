//
// model::quest
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
#[derive(Default, Model)]
#[model(getr, getmr, set)]
pub struct QuestDefinition {
    settings: Option<QuestSettings>,
    structure: QuestStructure,
    layers: QuestLayers,
    communities: QuestCommunities,
    journals: QuestJournals,
    entities: QuestEntities,
    items: QuestItems,
    resources: QuestResources,
    navdata: NavData,

    // filename and rawdata as string for all unsupported-by-editor definitions
    #[model(getr, add)]
    rawdata: Vec<(String, Vec<u8>)>,
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use community::QuestCommunities;
use entity::QuestEntities;
use item::QuestItems;
use journal::QuestJournals;
use layer::QuestLayers;
use navdata::NavData;
use questgraph::QuestStructure;
use resources::QuestResources;
use settings::QuestSettings;

use {Result, ValidatableElement};
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
impl QuestDefinition {
    // ------------------------------------------------------------------------
    #[allow(clippy::new_ret_no_self)]
    pub fn new(id: &str, caption: &str) -> Result<QuestDefinition> {
        use questgraph::segments;

        let mut new_def = QuestDefinition::default();

        let mut minimal_structure = QuestStructure::default();
        let mut root_segment = segments::QuestRootSegment::default();

        root_segment.set_start(segments::QuestStart::new("In"))?;
        root_segment.add_end(segments::QuestEnd::new("Out"))?;

        minimal_structure.set_root(root_segment)?;

        new_def.set_settings(QuestSettings::new(id, caption));
        new_def.set_structure(minimal_structure);

        Ok(new_def)
    }
    // ------------------------------------------------------------------------
    pub fn is_empty(&self) -> bool {
        self.settings.is_none() || self.structure.root().ok().is_none()
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// validation
// ----------------------------------------------------------------------------
impl ValidatableElement for QuestDefinition {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        self.structure
            .validate(&format!("{} quest structure", id))?;

        for block in self.structure.root()?.quest_blocks() {
            block.validate(&format!("{} root segment block", id))?;
        }

        for segment in self.structure.segments() {
            for block in segment.quest_blocks() {
                block.validate(&format!("{} segment \"{}\" block", id, segment.id()))?;
            }
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
