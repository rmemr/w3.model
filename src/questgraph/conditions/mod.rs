//
// questgraph::conditions
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
#[derive(Default, Model, QuestBlock)]
pub struct WaitUntil {
    #[model(getr)]
    id: BlockUid, // -> mapped to name
    #[model(getr)]
    guid: GUID,
    out: Links, // multiple outsockets possible, ids must match name
    #[model(getr, getmr)]
    conditions: Vec<Condition>, // of condition. condition <-> outsocketid is 1:1 relation

    #[model(set)]
    editordata: Option<EditorBlockData>,
}
// ----------------------------------------------------------------------------
// conditions
// ----------------------------------------------------------------------------
#[derive(Clone, Model)]
pub struct LogicCondition {
    #[model(getr)]
    operation: LogicOperation,
    #[model(iter, add)]
    conditions: Vec<Condition>,
}
// ----------------------------------------------------------------------------
#[derive(Clone, Model)]
#[model(getr)]
pub struct EnteredLeftAreaCondition {
    has_entered: bool,
    area: Option<LayerTagReference>,
    actor: Option<String>,
}
// ----------------------------------------------------------------------------
#[derive(Clone, Model)]
#[model(getr)]
pub struct InsideOutsideAreaCondition {
    is_inside: bool,
    area: Option<LayerTagReference>,
    actor: Option<String>,
}
// ----------------------------------------------------------------------------
#[derive(Clone)]
pub enum TimeCondition {
    After(Time),
    Before(Time),
    Time(Time),
    TimePeriod(Time, Time),
    Elapsed(HiResTime),
}
// ----------------------------------------------------------------------------
#[derive(Clone)]
pub enum InteractionType {
    Examine,
    Talk,
    Use,
    Loot,
    Custom(String),
}
// ----------------------------------------------------------------------------
#[derive(Clone, Model)]
#[model(getr)]
pub struct InteractionCondition {
    interaction: InteractionType,
    entity: Option<InteractionReference>,
}
// ----------------------------------------------------------------------------
#[derive(Clone, Model)]
#[model(getr)]
pub struct LoadingScreenCondition {
    is_shown: bool,
}
// ----------------------------------------------------------------------------
#[derive(Clone)]
pub enum Condition {
    FactsDb(FactsDbCondition),
    Logic(LogicCondition),
    Time(TimeCondition),
    EnteredLeftArea(EnteredLeftAreaCondition),
    InsideOutsideArea(InsideOutsideAreaCondition),
    Interaction(InteractionCondition),
    LoadingScreen(LoadingScreenCondition),
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use primitives::references::{InteractionReference, LayerTagReference};
use primitives::{HiResTime, Time, GUID};
use shared::{FactsDbCondition, LogicOperation};
use {Result, ValidatableElement};

use super::primitives::{
    BlockId, BlockName, BlockUid, EditorBlockData, InSocketId, Link, LinkTarget, Links,
    OutSocketId, SegmentId,
};
use super::{ManagedQuestBlock, QuestBlock, SegmentBlock};
// ----------------------------------------------------------------------------
impl WaitUntil {
    // ------------------------------------------------------------------------
    pub fn add_condition(&mut self, name: Option<&str>, mut condition: Condition) -> &mut Self {
        condition.set_name(name.unwrap_or("Out"));
        self.conditions.push(condition);
        self
    }
    // ------------------------------------------------------------------------
    pub fn set_condition(&mut self, mut condition: Condition) -> &mut Self {
        // only one condition -> name has to be Out
        condition.set_name("Out");
        self.conditions = vec![condition];
        self
    }
    // ------------------------------------------------------------------------
    pub fn clear_conditions(&mut self) -> &mut Self {
        self.conditions = vec![];
        self
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// conditions
// ----------------------------------------------------------------------------
impl Condition {
    // ------------------------------------------------------------------------
    pub fn name(&self) -> &str {
        match *self {
            Condition::FactsDb(ref cond) => cond.name(),
            _ => "Out",
        }
    }
    // ------------------------------------------------------------------------
    fn set_name(&mut self, name: &str) {
        if let Condition::FactsDb(ref mut cond) = *self {
            cond.set_name(name);
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl LogicCondition {
    // ------------------------------------------------------------------------
    pub fn new(operation: LogicOperation) -> LogicCondition {
        LogicCondition {
            operation,
            conditions: Vec::default(),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl EnteredLeftAreaCondition {
    // ------------------------------------------------------------------------
    pub fn new(
        actor: Option<String>,
        has_entered: bool,
        area: Option<LayerTagReference>,
    ) -> EnteredLeftAreaCondition {
        EnteredLeftAreaCondition {
            has_entered,
            area,
            actor,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl InsideOutsideAreaCondition {
    // ------------------------------------------------------------------------
    pub fn new(
        actor: Option<String>,
        is_inside: bool,
        area: Option<LayerTagReference>,
    ) -> InsideOutsideAreaCondition {
        InsideOutsideAreaCondition {
            is_inside,
            area,
            actor,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl InteractionCondition {
    // ------------------------------------------------------------------------
    pub fn new(
        interaction: InteractionType,
        entity: Option<InteractionReference>,
    ) -> InteractionCondition {
        InteractionCondition {
            interaction,
            entity,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl LoadingScreenCondition {
    // ------------------------------------------------------------------------
    pub fn new(is_shown: bool) -> LoadingScreenCondition {
        LoadingScreenCondition { is_shown }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Validation
// ----------------------------------------------------------------------------
use validator;
// ----------------------------------------------------------------------------
impl ValidatableElement for WaitUntil {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        if self.conditions.is_empty() {
            return Err(format!("{}: block does not contain any condition.", id));
        }

        for (i, cond) in self.conditions.iter().enumerate() {
            cond.validate(&format!("{} #{} condition", id, i + 1))?
        }

        // check condition <-> outsocketid match
        // assumption is:
        //  1:1 mapping between condition and its link target -> conditions name
        //  used as socketout id

        // not all condition HAVE to be connected to an outsocket (those conditions
        // will NOT be encoded!)
        // BUT all used out sockets have to match a condition

        for outsocket in self.out.keys().map(|socketid| socketid.0.as_str()) {
            if !self
                .conditions()
                .iter()
                .any(|cond| cond.name() == outsocket)
            {
                return Err(format!(
                    "{}: every outsocket id must match a condition. found socket without condition: {}",
                    id, outsocket
                ));
            }
        }
        //TODO deny "Out" socketid for multi-sockets to prevent problems?

        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for Condition {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        use self::Condition::*;

        match self {
            FactsDb(cond) => cond.validate(id),
            Logic(cond) => cond.validate(id),
            Time(cond) => cond.validate(id),
            EnteredLeftArea(cond) => cond.validate(id),
            InsideOutsideArea(cond) => cond.validate(id),
            Interaction(cond) => cond.validate(id),
            LoadingScreen(cond) => cond.validate(id),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for LogicCondition {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        // max no of conditions? no cascade of logic operations? max cascade?
        // waituntil.logic_condition_cascade:
        //   any:                         # alias for xor, "onlyOne" also works
        //     - factdb: [ c1, "=", 1 ]
        //     - factdb: [ c6, "=", 1 ]
        //     - factdb: [ c8, "=", 1 ]
        //     - factdb: [ c9, "=", 1 ]
        //   next:
        for (i, cond) in self.conditions.iter().enumerate() {
            cond.validate(&format!("{} #{} condition", id, i + 1))?;
            if let Condition::Logic(_) = cond {
                return Err(format!(
                    "{} #{} condition: nested logic conditions not supported",
                    id,
                    i + 1
                ));
            }
        }

        if self.conditions.len() < 2 || self.conditions.len() > 5 {
            Err(format!(
                "{}: logic condition requires 2-5 conditions. found: {}",
                id,
                self.conditions.len()
            ))
        } else {
            Ok(())
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for EnteredLeftAreaCondition {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        if let Some(ref actor) = self.actor {
            validator::validate_str_default(actor, id, "^[0-9_a-zA-Z]*$", "[a-z_0-9]")?;
        }
        // non empty area
        match self.area {
            Some(ref area) => area.validate(&format!("{} area", id))?,
            None => validate_err_missing!(id, "area"),
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for InsideOutsideAreaCondition {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        if let Some(ref actor) = self.actor {
            validator::validate_str_default(actor, id, "^[0-9_a-zA-Z]*$", "[a-z_0-9]")?;
        }
        // non empty area
        match self.area {
            Some(ref area) => area.validate(&format!("{} area", id))?,
            None => validate_err_missing!(id, "area"),
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for TimeCondition {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        match *self {
            TimeCondition::TimePeriod(ref from, ref to) => {
                if from.as_int() < to.as_int() {
                    Ok(())
                } else {
                    Err(format!(
                        "{}: expected \"from\" time to be before \"to\" time.",
                        id
                    ))
                }
            }
            _ => Ok(()),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for InteractionCondition {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        if let InteractionType::Custom(ref interaction) = self.interaction {
            validator::validate_str_default(
                interaction,
                &format!("{} custom interaction", id),
                "^[0-9_a-zA-Z]*$",
                "[a-z_0-9]",
            )?;
        }

        match self.entity {
            Some(ref entity) => entity.validate(&format!("{} interactive entity", id))?,
            None => validate_err_missing!(id, "interactive entity"),
        }

        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for LoadingScreenCondition {
    // ------------------------------------------------------------------------
    fn validate(&self, _id: &str) -> Result<()> {
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
