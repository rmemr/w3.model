//
// model::resources
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
#[derive(Default, ModelRepo)]
pub struct QuestResources {
    #[modelrepo(getr, add, has)]
    resources: IndexMap<String, GenericResource>,
}
// ----------------------------------------------------------------------------
#[derive(Model)]
pub struct GenericResource {
    #[model(getr)]
    id: String,
    // #[model(getr)]
    // data: DataInstance,
    #[model(set)]
    context: Option<String>,
    #[model(set)]
    metadata: Option<EncoderMetaData>,
}
// ----------------------------------------------------------------------------
#[derive(Default, Model)]
pub struct EncoderMetaData {
    #[model(set)]
    targetname: Option<String>,
    #[model(set)]
    targetpath: Option<String>,
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use indexmap::map::{Drain, IndexMap};

use {Result, ValidatableElement};
// ----------------------------------------------------------------------------
impl QuestResources {
    // ------------------------------------------------------------------------
    pub fn drain(&mut self) -> Drain<String, GenericResource> {
        self.resources.drain(..)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl GenericResource {
    // ------------------------------------------------------------------------
    pub fn new(id: &str) -> GenericResource {
        GenericResource {
            id: id.to_string(),
            context: None,
            // data,
            metadata: None,
        }
    }
    // ------------------------------------------------------------------------
    pub fn context(&self) -> &str {
        if let Some(ref meta) = self.metadata {
            if let Some(ref targetpath) = meta.targetpath {
                return targetpath;
            }
        }
        self.context.as_deref().unwrap_or("")
    }
    // ------------------------------------------------------------------------
    pub fn targetname(&self) -> &str {
        if let Some(ref meta) = self.metadata {
            if let Some(ref targetname) = meta.targetname {
                return targetname;
            }
        }
        &self.id
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Validation
// ----------------------------------------------------------------------------
use validator::validate_str_default;
// ----------------------------------------------------------------------------
impl ValidatableElement for GenericResource {
    fn validate(&self, id: &str) -> Result<()> {
        // targetname must have an extension so resource type can be properly
        // categorized
        let targetname = self.targetname();
        validate_str_default(targetname, id, "^[0-9_a-z.-]*$", "[a-z_0-9.]")?;

        if !targetname.contains('.') {
            return Err(format!(
                "{}: resource id or targetname must include file extension. found: {}",
                id, targetname
            ));
        }
        Ok(())
    }
}
// ----------------------------------------------------------------------------
impl ValidatableElement for EncoderMetaData {
    fn validate(&self, id: &str) -> Result<()> {
        if let Some(ref targetname) = self.targetname {
            validate_str_default(
                targetname,
                &format!("{}/targetname", id),
                "^[0-9_a-z.-]*$",
                "[a-z_0-9.]",
            )?;
            if !targetname.contains('.') {
                return Err(format!(
                    "{}/targetname: required file extension missing. found: {}",
                    id, targetname
                ));
            }
        }
        if let Some(ref targetpath) = self.targetpath {
            validate_str_default(
                targetpath,
                &format!("{}/targetpath", id),
                "^[0-9_a-z\\\\/]*$",
                "[a-z_0-9/]",
            )?
        }
        Ok(())
    }
}
// ----------------------------------------------------------------------------
