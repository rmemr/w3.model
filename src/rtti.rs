//
// model::rtti - custom rtti (yaml) definitions
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
#[derive(Default, Model)]
pub struct CustomRttiDefinition {
    #[model(getr, getmr)]
    classes: IndexMap<String, ClassRttiDefinition>,
}
// ----------------------------------------------------------------------------
#[derive(Model)]
pub struct ClassRttiDefinition {
    classname: String,
    #[model(set, getr)]
    baseclass: Option<String>,
    #[model(getr, getmr)]
    attributes: IndexMap<String, AttributeRttiType>,
}
// ----------------------------------------------------------------------------
#[derive(Copy, Clone)]
pub enum AssetReferenceRttiType {
    Area,
    Actionpoint,
    Mappin,
    Wanderpoint,
    Waypoint,
    Item,
    Loot,
}
// ----------------------------------------------------------------------------
#[derive(Copy, Clone)]
pub enum PrimitiveRttiType {
    Bool,
    Int,
    Name,
    NameRef(AssetReferenceRttiType),
    String,
    Float,
    TagList,
    Guid,
    EntityHandle,
    EntityHandleRef(AssetReferenceRttiType),
}
// ----------------------------------------------------------------------------
pub enum AttributeRttiType {
    Primitive(PrimitiveRttiType),
    Handle(String),
    Struct(String),
    Enum(String),
    ArrayPrimitives(PrimitiveRttiType),
    ArrayStructs(String),
    ArrayHandles(String),
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use indexmap::IndexMap;

use super::{Result, ValidatableElement};
// ----------------------------------------------------------------------------
impl CustomRttiDefinition {
    // ------------------------------------------------------------------------
    pub fn add_class(&mut self, classname: &str, rtti: ClassRttiDefinition) -> Result<()> {
        if self.classes.contains_key(classname) {
            Err(format!(
                "found duplicate class rtti definition for class: {}",
                classname
            ))
        } else {
            self.classes.insert(classname.to_string(), rtti);
            Ok(())
        }
    }
    // ------------------------------------------------------------------------
}
// -----------------------------------------------------------------------------
impl ClassRttiDefinition {
    // ------------------------------------------------------------------------
    pub fn new(name: &str) -> Self {
        Self {
            classname: name.to_string(),
            baseclass: None,
            attributes: IndexMap::default(),
        }
    }
    // ------------------------------------------------------------------------
    pub fn add_attribute(&mut self, name: &str, attrtype: AttributeRttiType) -> Result<()> {
        if self.attributes.contains_key(name) {
            Err(format!(
                "found duplicate attribute definition [{}] for class {}",
                name, self.classname
            ))
        } else {
            self.attributes.insert(name.to_string(), attrtype);
            Ok(())
        }
    }
    // ------------------------------------------------------------------------
}
// -----------------------------------------------------------------------------
use std::convert::TryFrom;

impl TryFrom<&str> for AssetReferenceRttiType {
    type Error = String;
    // ------------------------------------------------------------------------
    fn try_from(s: &str) -> Result<Self> {
        use self::AssetReferenceRttiType::*;

        match s {
            "area" => Ok(Area),
            "actionpoint" => Ok(Actionpoint),
            "mappin" => Ok(Mappin),
            "wanderpoint" => Ok(Wanderpoint),
            "waypoint" => Ok(Waypoint),
            "item" => Ok(Item),
            "loot" => Ok(Loot),
            _ => Err(format!("unknown asset reference rtti type: {}", s)),
        }
    }
    // ------------------------------------------------------------------------
}
// -----------------------------------------------------------------------------
// Validation
// ----------------------------------------------------------------------------
use validator;
// ----------------------------------------------------------------------------
impl ValidatableElement for CustomRttiDefinition {
    // ------------------------------------------------------------------------
    fn validate(&self, _id: &str) -> Result<()> {
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for ClassRttiDefinition {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        validator::validate_str(
            &self.classname,
            &format!("{} classname", id),
            &[
                validator::is_nonempty(),
                validator::chars(
                    "^[a-zA-Z_][0-9_a-zA-Z]*$",
                    "[a-zA-Z_0-9] and start with a character",
                ),
                validator::min_length(1),
                validator::max_length(250),
            ],
        )?;
        if let Some(baseclass) = self.baseclass.as_ref() {
            validator::validate_str_default(
                baseclass,
                &format!("{} baseclass", id),
                "^[0-9_a-zA-Z]*$",
                "[a-zA-Z_0-9]",
            )?;
        }
        // check names of attributes
        for attrname in self.attributes.keys() {
            validator::validate_str(
                attrname,
                &format!("{} variable name [{}]", id, attrname),
                &[
                    validator::is_nonempty(),
                    validator::chars(
                        "^[a-zA-Z_][0-9_a-zA-Z]*$",
                        "[a-zA-Z_0-9] and start with a character or _",
                    ),
                    validator::min_length(1),
                    validator::max_length(250),
                ],
            )?;
        }
        Ok(())
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ValidatableElement for AttributeRttiType {
    // ------------------------------------------------------------------------
    fn validate(&self, id: &str) -> Result<()> {
        use self::AttributeRttiType::*;

        match self {
            Handle(classname) | ArrayHandles(classname) => validator::validate_str_default(
                classname,
                &format!("{} variable type", id),
                "^[0-9_a-zA-Z]*$",
                "[a-zA-Z_0-9]",
            ),
            Enum(typename) => validator::validate_str_default(
                typename,
                &format!("{} enum type", id),
                "^[0-9_a-zA-Z]*$",
                "[a-zA-Z_0-9]",
            ),
            _ => Ok(()),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
