#![recursion_limit = "128"]
//
// w3model procedural macros to derives some common functionality by decalrations
//
extern crate proc_macro;
extern crate syn;
#[macro_use]
extern crate quote;
// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------

// functions tagged with `#[proc_macro_derive]` must currently reside in the
// root of the crate

// ----------------------------------------------------------------------------
// generic model accessor derives
// ----------------------------------------------------------------------------
#[proc_macro_derive(Model, attributes(model))]
pub fn model(input: TokenStream) -> TokenStream {
    let s = input.to_string();

    let ast = syn::parse_derive_input(&s).unwrap();
    let gen = model::model(&ast);

    gen.parse().unwrap()
}
// ----------------------------------------------------------------------------
#[proc_macro_derive(ModelRepo, attributes(modelrepo))]
pub fn model_repo(input: TokenStream) -> TokenStream {
    let s = input.to_string();

    let ast = syn::parse_derive_input(&s).unwrap();
    let gen = model_repo::model_repo(&ast);

    gen.parse().unwrap()
}
// ----------------------------------------------------------------------------
// questgraph derives
// ----------------------------------------------------------------------------
#[proc_macro_derive(QuestBlock, attributes(block))]
pub fn quest_in_out_block(input: TokenStream) -> TokenStream {
    let s = input.to_string();

    let ast = syn::parse_derive_input(&s).unwrap();
    let gen = questgraph::quest_in_out_block(&ast);

    gen.parse().unwrap()
}
// ----------------------------------------------------------------------------
#[proc_macro_derive(QuestOutBlock)]
pub fn quest_out_block(input: TokenStream) -> TokenStream {
    let s = input.to_string();

    let ast = syn::parse_derive_input(&s).unwrap();
    let gen = questgraph::quest_out_block(&ast);

    gen.parse().unwrap()
}
// ----------------------------------------------------------------------------
#[proc_macro_derive(QuestInBlock)]
pub fn quest_in_block(input: TokenStream) -> TokenStream {
    let s = input.to_string();

    let ast = syn::parse_derive_input(&s).unwrap();
    let gen = questgraph::quest_in_block(&ast);

    gen.parse().unwrap()
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use proc_macro::TokenStream;
// ----------------------------------------------------------------------------
#[macro_use]
mod helpers;
mod model;
mod model_repo;
mod questgraph;
// ----------------------------------------------------------------------------
